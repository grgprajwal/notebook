package com.grgprajwal26.notebook;

/**
 * Created by black_flame on 2/20/2017.
 */
public class Note {
    private String title, message;
    private long noteId, dateCreatedMilli;
    public enum Category {PERSONAL, TECHNICAL, QUOTE, FINANCE};
    private Category category;


    public Note(String title, String message, Category category){
        this.title = title;
        this.message = message;
        this.category = category;
        this.noteId = 0;
        this.dateCreatedMilli = 0;

    }

    public Note(String title, String message, Category category,long noteId, long dateCreatedMilli){
        this.title = title;
        this.message = message;
        this.category = category;
        this.noteId = noteId;
        this.dateCreatedMilli = dateCreatedMilli;
    }

    public String getTitle(){
        return title;
    }

    public String getMessage(){
        return message;
    }

    public Category getCategory(){
        return category;
    }
    public long getNoteId(){
        return noteId;
    }
    public long getDateCreatedMilli(){
        return dateCreatedMilli;
    }

    public String toString(){
        return "ID: "+noteId+" Title: "+ title + " Message: "+ message+" IconId: "+category.name() + " DateCreated: "+
                dateCreatedMilli;
    }

    public int getAssociatedDrawable(){
        return categoryToDrawable(category);
    }

    public static int categoryToDrawable(Category noteCategory){
        switch (noteCategory){
            case PERSONAL:
                return R.drawable.bg;
            case TECHNICAL:
                return R.drawable.bird;
            case QUOTE:
                return R.drawable.bird2;
            case FINANCE:
                return R.drawable.playbtn;
        }
        return R.drawable.playbtn;
    }
}
