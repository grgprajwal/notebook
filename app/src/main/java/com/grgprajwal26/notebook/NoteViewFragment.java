package com.grgprajwal26.notebook;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class NoteViewFragment extends Fragment {


    public NoteViewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View fragmentLayout = inflater.inflate(R.layout.fragment_note_view,container, false);

        TextView title = (TextView) fragmentLayout.findViewById(R.id.noteDetailTitle);
        TextView message = (TextView) fragmentLayout.findViewById(R.id.noteDetailBody);
        ImageView noteIcon = (ImageView) fragmentLayout.findViewById(R.id.detailNoteIcon);

        Intent intent = getActivity().getIntent();

        title.setText(intent.getStringExtra(MainActivity.NOTE_TITLE));
        message.setText(intent.getStringExtra(MainActivity.NOTE_MESSAGE));

        Note.Category notecat = (Note.Category) intent.getSerializableExtra(MainActivity.NOTE_CATEGORY);
        noteIcon.setImageResource(Note.categoryToDrawable(notecat));




        // Inflate the layout for this fragment
        return fragmentLayout;
    }

}
