package com.grgprajwal26.notebook;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class NoteDetailActivity extends AppCompatActivity {


    public static final String NEW_NOTE_EXTRA = "New Note";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_detail);

        createAndAddFragment();
    }


    private void createAndAddFragment(){
        //grab intent
        Intent intent = getIntent();
        //grab fragmenttoLaunch type
        MainActivity.FragmentToLaunch fragmentToLaunch = (MainActivity.FragmentToLaunch) intent.getSerializableExtra(MainActivity.NOTE_FRAGMENT_TO_LOAD);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();


        //choose correct fragment to load
        switch (fragmentToLaunch){
            case VIEW:
                //create and add note view fragment
                setTitle(R.string.note_detail_title);
                NoteViewFragment noteViewFragment = new NoteViewFragment();
                fragmentTransaction.add(R.id.noteContainer,noteViewFragment,"NOTE_DETAIL_FRAGMENT");
                break;
            case EDIT:
                //create and add note edit fragment
                setTitle(R.string.note_edit_title);
                NoteEditFragment noteEditFragment = new NoteEditFragment();
                fragmentTransaction.add(R.id.noteContainer,noteEditFragment,"NOTE_EDIT_FRAGMENT");
                break;
            case CREATE:
                //to add new notes
                NoteEditFragment noteCreateFragment = new NoteEditFragment();
                setTitle(R.string.create_note_title);
                Bundle bundle = new Bundle();
                bundle.putBoolean(NEW_NOTE_EXTRA,true);
                noteCreateFragment.setArguments(bundle);
                fragmentTransaction.add(R.id.noteContainer,noteCreateFragment,"NOTE_CREATE_FRAGMENT");
                break;

        }

        //commmit the fragment
        fragmentTransaction.commit();
    }
}
