package com.grgprajwal26.notebook;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by black_flame on 2/20/2017.
 */
public class NoteAdapter extends ArrayAdapter<Note> {

    public static class ViewHolder{
        TextView title, note;
        ImageView noteIcon;
    }

    public NoteAdapter(Context context, ArrayList<Note> notes){
        super(context,0,notes);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        //get item of that position
        Note note = getItem(position);

        ViewHolder viewHolder;

        //If existing view is reused, otherwise inflate a new view
        if (convertView == null){

            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_row_layout, parent, false);


            viewHolder  = new ViewHolder();
            viewHolder.title = (TextView) convertView.findViewById(R.id.listTitle);
            viewHolder.note = (TextView)convertView.findViewById(R.id.listBody);
            viewHolder.noteIcon = (ImageView) convertView.findViewById(R.id.listImage);

            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }


        //fill each reference view  with data associated with the note
        viewHolder.title.setText(note.getTitle());
        viewHolder.note.setText(note.getMessage());
        viewHolder.noteIcon.setImageResource(note.getAssociatedDrawable());

        return convertView;
    }
}
