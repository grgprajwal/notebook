package com.grgprajwal26.notebook;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;


/**
 * A simple {@link Fragment} subclass.
 */
public class NoteEditFragment extends Fragment {

    private ImageButton noteCatButton;
    private Note.Category savedButtonCategory;
    private AlertDialog categoryDialogObject,confirmDialogObject;
    private EditText title, body;
    private static final String MODIFIED_CATEGORY="Modified Category";
    private boolean newNote=false;
    private long noteId ;

    public NoteEditFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //grab the bundle which is used for creatingnew note
        Bundle bundle = this.getArguments();
        if(bundle!=null){
            //if bundle have value of the name newnoteextra then grab else use false as the new note value;
            newNote = bundle.getBoolean(NoteDetailActivity.NEW_NOTE_EXTRA, false);
        }


        //for orientation change we load the saved icon
        if(savedInstanceState!=null){
            savedButtonCategory = (Note.Category) savedInstanceState.get(MODIFIED_CATEGORY);
        }
        View fragmentLayout = inflater.inflate(R.layout.fragment_note_edit,container,false);

        title = (EditText) fragmentLayout.findViewById(R.id.noteEditDetailTitle);
        body = (EditText) fragmentLayout.findViewById(R.id.noteEditDetailBody);
        noteCatButton = (ImageButton) fragmentLayout.findViewById(R.id.editIconButton);
        Button saveButton = (Button)fragmentLayout.findViewById(R.id.saveEdit);

        Intent intent = getActivity().getIntent();

        title.setText(intent.getExtras().getString(MainActivity.NOTE_TITLE," "));
        body.setText(intent.getExtras().getString(MainActivity.NOTE_MESSAGE," "));
        noteId = intent.getExtras().getLong(MainActivity.NOTE_ID, 0);

        //if orientation changes

        //if the icon is saved in noteCatButton then we load that
        if(savedButtonCategory!= null){
            noteCatButton.setImageResource(Note.categoryToDrawable(savedButtonCategory));
        }
        //if the icon is not saved in noteCatButton then we load new icon
        else if(!newNote){
            Note.Category noteCat = (Note.Category) intent.getSerializableExtra(MainActivity.NOTE_CATEGORY);
            savedButtonCategory = noteCat;
            noteCatButton.setImageResource(Note.categoryToDrawable(noteCat));
        }

        buildCategoryDialog();
        buildConfirmDialog();

        //on clicking the note image button
        noteCatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categoryDialogObject.show();
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDialogObject.show();
            }
        });

        return fragmentLayout;



    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putSerializable(MODIFIED_CATEGORY, savedButtonCategory);
    }

    private void buildCategoryDialog(){
        final String[] categories = new String[]{"Personal","Technical","Quote","Finance"};
        AlertDialog.Builder categoryBuilder = new AlertDialog.Builder(getActivity());
        categoryBuilder.setTitle("Choose Note Type");
        categoryBuilder.setSingleChoiceItems(categories, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                //dismiss our dialog object
                categoryDialogObject.cancel();

                switch (item){
                    case 0:
                        savedButtonCategory = Note.Category.PERSONAL;
                        noteCatButton.setImageResource(Note.categoryToDrawable(savedButtonCategory));
                        break;
                    case 1:
                        savedButtonCategory = Note.Category.TECHNICAL;
                        noteCatButton.setImageResource(Note.categoryToDrawable(savedButtonCategory));
                        break;
                    case 2:
                        savedButtonCategory = Note.Category.QUOTE;
                        noteCatButton.setImageResource(Note.categoryToDrawable(savedButtonCategory));
                        break;
                    case 3:
                        savedButtonCategory = Note.Category.FINANCE;
                        noteCatButton.setImageResource(Note.categoryToDrawable(savedButtonCategory));
                        break;
                }
            }
        });

        categoryDialogObject = categoryBuilder.create();

    }

    private void buildConfirmDialog(){
        AlertDialog.Builder confirmBuilder = new AlertDialog.Builder(getActivity());
        confirmBuilder.setTitle("Are You Sure?");
        confirmBuilder.setMessage("Are you sure you want to save the note?");
        confirmBuilder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d("Save Note", "Note title: "+ title.getText()+" Note Message: "+ body.getText());
                NotebookDbAdapter dbAdapter = new NotebookDbAdapter(getActivity().getBaseContext());
                dbAdapter.open();
                // if new note create new note
                if(newNote){
                    dbAdapter.createNote(title.getText()+"",body.getText()+"",(savedButtonCategory==null)?Note.Category.PERSONAL:savedButtonCategory);



                }
                //else it is in edit page
                else{

                    dbAdapter.updateNote(noteId,title.getText()+"",body.getText()+"",savedButtonCategory);

                }
                dbAdapter.close();
                Intent intent = new Intent(getActivity(),MainActivity.class);
                startActivity(intent);
            }
        });

        confirmBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //do nothing
                Log.d("do nothig here","NO need to do anything");
            }
        });

        confirmDialogObject = confirmBuilder.create();
    }
}
